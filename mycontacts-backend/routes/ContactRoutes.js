const express = require("express")
const router = express.Router()
const { getContacts, createContact, contactDetails, updateContact, deleteContact } = require("../controllers/ContactController")
const validateToken = require("../middleware/ValidateTokenHandler")

router.use(validateToken)
router.route('/').get(getContacts).post(createContact)
router.route('/:id').get(contactDetails).put(updateContact).delete(deleteContact)

module.exports = router
