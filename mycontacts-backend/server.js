const express = require("express");
const connectDb = require("./config/DbConnection")
const errorHandler = require("./middleware/ErrorHandler");
const dotenv = require("dotenv").config();

connectDb();
const app = express();

const port = process.env.PORT || 5000;

app.use(express.json());
app.use("/api/contacts", require("./routes/ContactRoutes")); // 'use' used for adding middleware, this line establishes the base path
app.use("/api/users", require("./routes/UserRoutes")); // 'use' used for adding middleware, this line establishes the base path
app.use(errorHandler);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
